var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var sintaxSCSS = require('postcss-scss');
var autoprefixer = require('autoprefixer');
var stylelint = require("stylelint");
var stylelintConfigStandar = require("stylelint-config-standard");
var reporter = require("postcss-reporter");
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');

function task(){
    var processors = [ autoprefixer({browsers: ['last 4 versions', 'ie >= 8']}), 
                       stylelint(stylelintConfigStandar),
                       reporter({ clearMessages: true, throwError: false })
                     ];
    
    
    gutil.log('Start build'); 
    return gulp.src('./src/css/main.scss')
        .pipe(plumber())
        .pipe(postcss(processors, {syntax: sintaxSCSS}))
        
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css'))
        .on('end', function(){ 
            gutil.log('End Build'); 
         });
}

gulp.task('default', function () {
    watch('./src/css/*.scss', function () {
        task();
    });
    return task();
});


